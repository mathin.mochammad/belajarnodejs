'use strict'
const knex = require('../conn')
const response = require('../res')

exports.index = async (req, res) => {
    try {
        let buku = await knex('buku');
        response.ok(buku, res);
    } catch (e) {
        console.log(e);
    }
}

exports.show = async (req, res) => {
    try {
        let id = req.params.id
        let buku = await knex('buku').where('id', id)

        response.ok({
            buku
        }, res)
    } catch (e) {
        console.log(e);
        next(e);
    }
}

exports.store = async (req, res) => {
    try {
        let judul = req.body.judul;
        let sinopsis = req.body.sinopsis;
        let penulis = req.body.penulis;

        let id = await knex('buku').insert({
            'judul': judul,
            'sinopsis': sinopsis,
            'penulis': penulis
        })
            
        response.ok({
            id: id[0],
            judul,
            sinopsis,
            penulis
        }, res);

    } catch (e) {
        console.log(e);
        next(e);
    }
}

exports.update = async (req, res) => {
    try {
        let id = req.params.id;
        let judul = req.body.judul;
        let sinopsis = req.body.sinopsis;
        let penulis = req.body.penulis;

        await knex('buku').where('id', id).update({
            'judul': judul,
            'sinopsis': sinopsis,
            'penulis': penulis
        })

        response.ok({
            id: id[0],
            judul,
            sinopsis,
            penulis
        }, res)
    } catch (e) {
        console.log(e);
        next(e);
    }
}

exports.destroy = async (req, res) => {
    try {
        let id = req.params.id;
        
        await knex('buku').where('id', id).del()        
        response.ok({
            id,
        }, res)
    } catch (e) {
        console.log(e);
        next(e)
    }
}
