'use strict';

const response = require('./res');
const knex = require('./conn');

exports.index = function(req, res) {
    response.ok(`Hello from the Node JS RESTful side! ${process.env.PORT}`, res)
};

