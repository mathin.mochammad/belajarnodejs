const express = require('express');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const app = express();
dotenv.config();

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(bodyParser.json({
    limit: "8mb"
}))

const catalogRouter = require('./routes/catalog')

app.use('/catalog', catalogRouter)

app.listen(port, () => console.log(`ELearn Node JS With Kiddy, RESTful API server started on port : ${port}!`))