const express = require('express')
const router = express.Router()


const buku_controller = require('../controllers/bukuController')

// Router Catalog Buku
router.get('/', buku_controller.index)
router.post('/buku', buku_controller.store)
router.get('/buku/:id', buku_controller.show)
router.put('/buku/:id', buku_controller.update)
router.delete('/buku/:id', buku_controller.destroy)

module.exports = router;